# domblocker

A small Bash script for quickly adding and removing domain-blocking rules to iptables. Designed for blocking federation with other servers in PeerTube.